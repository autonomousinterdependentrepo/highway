import {StatementType}         from './ast/statement/Statement'
import {packag}                from './ast/structure/Package'
import {DeserializerGenerator} from './generator/DeserializerGenerator'
import {ModelGenerator}        from './generator/ModelGenerator'

var fs = require('fs');

const modelGenerator = new ModelGenerator()

const deserialize = packag('model', 'bitbucket.org/votecube/votecube-crud/model')

const schemaString = fs.readFileSync('../../../votecube-ui/schemas/relational-db/src/generated/schema.json', 'utf8');

const schema = JSON.parse(schemaString)

console.log(modelGenerator.generate(schema).src(deserialize, StatementType.FILE_DEFINITION))
