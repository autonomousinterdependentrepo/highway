import {DI}              from '@airport/di'
import {
	EntityRelationType,
	JsonSchema,
	SQLDataType
}                         from '@airport/ground-control'
import {IASTUnit}         from '../ast/ASTUnit'
import {
	IVariable,
	variable
}                         from '../ast/expression/constructs/Variable'
import {array}            from '../ast/expression/type/Array'
import {Pointer}          from '../ast/expression/type/Pointer'
import {struct}           from '../ast/expression/type/Struct'
import {
	boolType,
	int64Type,
	stringType
}                         from '../ast/expression/type/PrimitiveType'
import {IType}            from '../ast/expression/type/Type'
import {comment}          from '../ast/statement/Comment'
import {IStatement}       from '../ast/statement/Statement'
import {structDefinition} from '../ast/statement/StructDefinition'
import {file}             from '../ast/structure/File'
import {packag}           from '../ast/structure/Package'
import {MODEL_GENERATOR}  from '../diTokens'
import {
	Generator,
	IGenerator
}                         from './Generator'

export interface IModelGenerator
	extends IGenerator {

}

export class ModelGenerator
	extends Generator
	implements IModelGenerator {

	generate(
		schema: JsonSchema
	): IASTUnit {
		const modelPkg = packag('model', 'bitbucket.org/votecube/votecube-crud/model')

		const statements: IStatement[] = [
			comment(`Votecube schema.`)
		]

		for (let entity of schema.versions[0].entities) {
			const classProperties: IVariable[] = []
			for (let property of entity.properties) {
				let type: IType
				if (property.relationRef) {
					const relation = entity.relations[property.relationRef.index]
					let isArray    = false
					switch (relation.relationType) {
						case EntityRelationType.ONE_TO_MANY: {
							isArray = true
							break
						}
						case EntityRelationType.MANY_TO_ONE: {
							break
						}
					}

					// TODO: add support for other schemas
					// relation.relationTableSchemaIndex
					const relationEntity = schema.versions[0].entities[relation.relationTableIndex]

					type = struct(relationEntity.name, modelPkg)

					if (isArray) {
						type = new Pointer(array(type))
					} else {
						type = new Pointer(type)
					}
				} else {
					const column = entity.columns[property.columnRef.index]
					switch (column.type) {
						case SQLDataType.BOOLEAN: {
							type = boolType()
							break
						}
						case SQLDataType.DATE:
						case SQLDataType.NUMBER: {
							type = int64Type()
							break
						}
						case SQLDataType.ANY:
						case SQLDataType.JSON:
						case SQLDataType.STRING: {
							type = stringType()
							break
						}
					}
				}
				let firstLetter     = property.name.substring(0, 1).toUpperCase()
				let remainder       = property.name.substring(1, property.name.length)
				let propertyName    = firstLetter + remainder
				const classProperty = variable(propertyName, type, modelPkg, `json:"${property.name}"`)
				classProperties.push(classProperty)
			}

			const modelStruct = struct(entity.name, modelPkg, classProperties)

			statements.push(structDefinition(modelStruct))
		}

		const sourceFile = file(
			'Schema',
			modelPkg,
			[],
			statements
		)
		return sourceFile
	}

}

DI.set(MODEL_GENERATOR, ModelGenerator)
