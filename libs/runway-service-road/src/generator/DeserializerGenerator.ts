import {DI}         from '@airport/di'
import {JsonSchema} from '@airport/ground-control'
import {
	_else,
	_if,
	_return,
	byteType,
	byteVar,
	struct,
	classVar,
	comment,
	equals,
	error,
	errorVar,
	file,
	func,
	IASTUnit,
	imprt,
	int64,
	int64Type,
	NIL,
	notEquals,
	packag,
	pointerVar,
	stringVar,
	tuple,
	tupleDef,
	varDef
}                   from '../ast/ast'
import {call}                   from '../ast/expression/FunctionCall'
import {ptr}                    from '../ast/expression/type/Pointer'
import {assign}                 from '../ast/statement/Assignment'
import {funcRef}                from '../ast/structure/FunctionReference'
import {DESERIALIZER_GENERATOR} from '../diTokens'
import {
	Generator,
	IGenerator
}                               from './Generator'

export interface IDeserializerGenerator
	extends IGenerator {

}

export class DeserializerGenerator
	extends Generator
	implements IDeserializerGenerator {

	generate(
		schema: JsonSchema
	): IASTUnit {
		const fmtPkg                       = packag('fmt')
		const nullPkg                      = packag('fmt', 'github.com/volatiletech')
		const modelsPkg                    = packag('models', 'bitbucket.org/votecube/votecube-crud')
		const modelPkg                     = packag('model', 'bitbucket.org/votecube/votecube-crud/deserialize')
		const deserializePkg               = packag('deserialize', 'bitbucket.org/votecube/votecube-crud')
		const Factor                       = struct('Factor', modelsPkg)
		const CreatePollDeserializeContext = struct('CreatePollDeserializeContext', deserializePkg)

		const REFERENCE = byteVar('REFERENCE', deserializePkg)

		const RByte = funcRef(deserializePkg, 'RByte', [
			pointerVar('ctx', ptr(CreatePollDeserializeContext)),
			errorVar('err')
		], tupleDef(byteType(), error()))

		const RNum = funcRef(deserializePkg, 'RNum', [
			pointerVar('ctx', ptr(CreatePollDeserializeContext)),
			errorVar('err')
		], tupleDef(int64Type(), error()))

		var ctx, err
		var factor, factorDef
		var objectType, objectTypeDef

		var factorId, factorIdRef, name, nameRef

		const sourceFile = file(
			'Factor',
			modelPkg,
			[
				imprt(fmtPkg),
				imprt(deserializePkg),
				imprt(modelsPkg),
				imprt(nullPkg)
			],
			[
				comment(`The order of serialized properties is fixed at property index of entity.`),
				func(modelPkg, 'DeserializeFactor', [
					ctx = pointerVar('ctx', ptr(CreatePollDeserializeContext)),
					err = errorVar('err')
				], tupleDef(Factor, int64Type(), error()), [
					factorDef = varDef(factor = classVar('factor', Factor)),
					_if(notEquals(err, NIL), [
						_return(tuple(factor, int64(0), err))
					]),
					objectTypeDef = varDef(objectType = byteVar('objectType')),
					assign([objectType, err], call(RByte, [ctx, err])),
					[_if(equals(objectType, REFERENCE), [
						factorIdRef = varDef(factorId = byteVar('factorId')),
						assign([factorId, err], call(RNum, [ctx, err])),
					]), _else([
						nameRef = varDef(name = stringVar('name'))
					])]
				])
			]
		)

		return sourceFile
	}


}

DI.set(DESERIALIZER_GENERATOR, DeserializerGenerator)
