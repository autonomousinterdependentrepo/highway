import {JsonSchema} from '@airport/ground-control'
import {ASTUnit}    from '../ast/ASTUnit'

export interface IGenerator {

	generate(
		schema: JsonSchema
	): ASTUnit
}

export abstract class Generator {

	abstract generate(
		schema: JsonSchema
	): ASTUnit

}
