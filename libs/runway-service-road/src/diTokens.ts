import {system}                     from '@airport/di'
import {JsonSchema}                 from '@airport/ground-control'
import {IASTUnit}                   from './ast/ASTUnit'
import {IDeserializerGenerator}     from './generator/DeserializerGenerator'
import {IGenerator}                 from './generator/Generator'
import {IPersistenceLayerGenerator} from './PersistenceLayerGenerator'

const runwayServiceRoad = system('highway').lib('runway-service-road')

export const PERSISTENCE_LAYER_GENERATOR = runwayServiceRoad.token<IPersistenceLayerGenerator>()
export const DESERIALIZER_GENERATOR      = runwayServiceRoad.token<IDeserializerGenerator>()
export const MODEL_GENERATOR      = runwayServiceRoad.token<IDeserializerGenerator>()
