import {DI}                          from '@airport/di'
import {PortableQuery}               from '@airport/ground-control'
import {PERSISTENCE_LAYER_GENERATOR} from './diTokens'

export interface IPersistenceLayerGenerator {

	generateServerPersistenceLayer(
		transactionalRecords: PortableQuery[][]
	): Promise<void>

}

export class PersistenceLayerGenerator
	implements IPersistenceLayerGenerator {

	async generateServerPersistenceLayer(
		transactions: PortableQuery[][]
	): Promise<void> {
		for (const transaction of transactions) {
			// each transaction is an array of persistence operations, executed in that transaction in
			// order
			for (const portableQuery of transaction) {
				// Examine the query and generate (as needed) the code for:
				// 1) (de)serialization - only needed once per entity
				// 2) validation - only needed once per entity
				// 3) persistence - unique to queries, may be reusable for inserts
				//       (updates/upserts are a future story)
				// 4) execution harness (puts it all together, allows for customizations)

				await this.generateSerializationCode(portableQuery)
				await this.generateValidationCode(portableQuery)
				await this.generatePersistenceCode(portableQuery)
				await this.generateExecutionHarness(transaction)
			}
		}
	}

	private async generateSerializationCode(
		portableQuery: PortableQuery
	): Promise<void> {

	}

	private async generateValidationCode(
		portableQuery: PortableQuery
	): Promise<void> {

	}

	private async generatePersistenceCode(
		portableQuery: PortableQuery
	): Promise<void> {

	}

	private async generateExecutionHarness(
		transaction: PortableQuery[]
	): Promise<void> {

	}

}

DI.set(PERSISTENCE_LAYER_GENERATOR, PersistenceLayerGenerator)