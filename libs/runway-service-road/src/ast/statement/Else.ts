import {IPackage} from '../structure/Package'
import {
	Block,
	BlockType,
	IBlock
}                 from './Block'
import {IElseIf}  from './ElseIf'
import {IIf}      from './If'
import {
	IStatement,
	StatementType
}                 from './Statement'

export interface IElse
	extends IBlock {

	is(
		blockType: BlockType
	): boolean

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class Else
	extends Block
	implements IElse {

	is(
		blockType: BlockType
	): boolean {
		return blockType === BlockType.ELSE
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		return `else ` + super.src(packageOfSource, statementType)
	}

}

export function _else(
	statements: (IStatement | (IIf | IElseIf | IElse)[])[]
): IElse {
	return new Else(StatementType.BLOCK, statements)
}