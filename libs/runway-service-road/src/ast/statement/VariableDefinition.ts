import {IVariable} from '../expression/constructs/Variable'
import {IPackage}  from '../structure/Package'
import {
	DOES_NOT_RETURN,
	IReturnType
}                  from './Return'
import {
	IStatement,
	Statement,
	StatementType
}                  from './Statement'

export interface IVariableDefinition<V extends IVariable>
	extends IStatement {

	variable: V

	src(
		packageOfSource: IPackage
	): string

}

export class VariableDefinition<V extends IVariable>
	extends Statement
	implements IVariableDefinition<V> {

	constructor(
		public variable: V
	) {
		super(StatementType.DEFINITION)
		variable.declared = true
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return DOES_NOT_RETURN
	}

	src(
		packageOfSource: IPackage
	): string {
		return this.variable.src(packageOfSource, this.type)
	}

}

export function varDef<V extends IVariable>(
	variable: V
): IVariableDefinition<V> {
	return new VariableDefinition(variable)
}
