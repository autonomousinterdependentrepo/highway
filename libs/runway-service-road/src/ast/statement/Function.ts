import {IVariable} from '../expression/constructs/Variable'
import {IType}     from '../expression/type/Type'
import {IPackage}  from '../structure/Package'
import {
	Block,
	BlockType,
	IBlock
}                  from './Block'
import {IElse}     from './Else'
import {IElseIf}   from './ElseIf'
import {IIf}       from './If'
import {
	DOES_NOT_RETURN,
	IReturnType,
	VOID
}                  from './Return'
import {
	IStatement,
	Statement,
	StatementType
}                  from './Statement'

export interface IFunction
	extends IBlock {

	name: string,
	sourcePackage: IPackage
	paramDefs: IParameterDefinitions
	typeOfReturn: IType

	is(
		blockType: BlockType
	): boolean

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		sourcePackage: IPackage,
		statementType: StatementType
	): string

}

export class Function
	extends Block
	implements IFunction {

	paramDefs: IParameterDefinitions

	constructor(
		public sourcePackage: IPackage,
		public name: string,
		parameters: IVariable[]         = [],
		public typeOfReturn: IType      = null,
		statements: (IStatement | (IIf | IElseIf | IElse)[])[] = []
	) {
		super(StatementType.DEFINITION, statements)
		this.paramDefs = new ParameterDefinitions(parameters)
	}

	is(
		blockType: BlockType
	): boolean {
		return blockType === BlockType.FUNCTION
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return DOES_NOT_RETURN
	}

	src(
		sourcePackage: IPackage,
		statementType: StatementType
	): string {
		switch (statementType) {
			case StatementType.FILE_DEFINITION:
				let returnDefinition = ''
				if (this.typeOfReturn) {
					returnDefinition = ' ' +
						this.typeOfReturn.src(sourcePackage, StatementType.PARAMETER_DEFINITION)
				}
				return `
func ${this.name}${this.paramDefs.src(sourcePackage, statementType)}${returnDefinition} `
			+ super.src(sourcePackage, statementType)
			default:
				throw new Error(`Unsupported StatementType ${statementType} for Function.`)
		}
	}

}

export interface IParameterDefinitions
	extends IStatement {
	parameters: IVariable[]

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		sourcePackage: IPackage,
		statementType: StatementType
	): string

}

export class ParameterDefinitions
	extends Statement
	implements IParameterDefinitions {

	constructor(
		public parameters: IVariable[]
	) {
		super(StatementType.PARAMETER_DEFINITION)
		for(const parameter of parameters) {
			parameter.declared = true
		}
	}

	src(
		sourcePackage: IPackage,
		statementType: StatementType
	): string {
		return `(${this.parameters.map(
			parameter => parameter.src(sourcePackage, this.type)).join(', ')})`
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return DOES_NOT_RETURN
	}

}

export function func(
	sourcePackage: IPackage,
	name: string,
	parameters?: IVariable[],
	returnType?: IType,
	statements?: (IStatement | (IIf | IElseIf | IElse)[])[]
): IFunction {
	return new Function(sourcePackage, name, parameters, returnType, statements)
}
