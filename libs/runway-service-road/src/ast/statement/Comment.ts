import {IPackage} from '../structure/Package'
import {
	DOES_NOT_RETURN,
	IReturnType
}                 from './Return'
import {
	IStatement,
	Statement,
	StatementType
}                 from './Statement'

export interface IComment
	extends IStatement {

	text: string

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class Comment
	extends Statement
	implements IComment {

	constructor(
		public text: string
	) {
		super(StatementType.DEFINITION)
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return DOES_NOT_RETURN
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		return `
/**
	${this.text}
*/`
	}

}


export function comment(
	text: string
): IComment {
	return new Comment(text)
}
