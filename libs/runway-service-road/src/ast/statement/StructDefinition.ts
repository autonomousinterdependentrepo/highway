import {IVariable} from '../expression/constructs/Variable'
import {IStruct}   from '../expression/type/Struct'
import {IPackage}  from '../structure/Package'
import {
	DOES_NOT_RETURN,
	IReturnType
}                  from './Return'
import {
	IStatement,
	Statement,
	StatementType
}                  from './Statement'

export interface IStructDefinition<S extends IStruct>
	extends IStatement {

	struct: S

	src(
		packageOfSource: IPackage
	): string

}

export class StructDefinition<S extends IStruct>
	extends Statement
	implements IStructDefinition<S> {

	constructor(
		public struct: S
	) {
		super(StatementType.STRUCT_DEFINITION)
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return DOES_NOT_RETURN
	}

	src(
		packageOfSource: IPackage
	): string {
		return this.struct.src(packageOfSource, this.type)
	}

}

export function structDefinition<S extends IStruct>(
	struct: S
): IStructDefinition<S> {
	return new StructDefinition(struct)
}
