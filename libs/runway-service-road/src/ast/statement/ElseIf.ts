import {IBooleanExpression} from '../expression/BooleanExpression'
import {IPackage}           from '../structure/Package'
import {BlockType}          from './Block'
import {IElse}              from './Else'
import {
	If,
	IIf
}                           from './If'
import {
	IStatement,
	StatementType
}                           from './Statement'

export interface IElseIf
	extends IIf {

	is(
		blockType: BlockType
	): boolean

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class ElseIf
	extends If
	implements IElseIf {

	is(
		blockType: BlockType
	): boolean {
		return blockType === BlockType.ELSE_IF
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		return `else ` + super.src(packageOfSource, statementType)
	}

}

export function _elseIf(
	booleanExpression: IBooleanExpression<any, any>,
	statements: (IStatement | (IIf | IElseIf | IElse)[])[]
): IIf {
	return new If(booleanExpression, statements)
}
