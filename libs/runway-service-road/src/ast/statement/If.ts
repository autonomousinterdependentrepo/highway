import {IBooleanExpression} from '../expression/BooleanExpression'
import {IPackage}           from '../structure/Package'
import {
	Block,
	BlockType,
	IBlock
}                           from './Block'
import {IElse}              from './Else'
import {IElseIf}            from './ElseIf'
import {
	IStatement,
	StatementType
}                           from './Statement'

export interface IIf
	extends IBlock {

	booleanExpression: IBooleanExpression<any, any>

	is(
		blockType: BlockType
	): boolean

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class If
	extends Block
	implements IIf {

	is(
		blockType: BlockType
	): boolean {
		return blockType === BlockType.IF
	}

	constructor(
		public booleanExpression: IBooleanExpression<any, any>,
		statements: (IStatement | (IIf | IElseIf | IElse)[])[]
	) {
		super(StatementType.BLOCK, statements)
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		return `if ${this.booleanExpression.src(packageOfSource, StatementType.BOOLEAN)} `
			+ super.src(packageOfSource, statementType)
	}

}

export function _if(
	booleanExpression: IBooleanExpression<any, any>,
	statements: (IStatement | (IIf | IElseIf | IElse)[])[]
): IIf {
	return new If(booleanExpression, statements)
}
