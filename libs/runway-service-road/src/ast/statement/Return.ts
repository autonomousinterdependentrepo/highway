import {IExpression} from '../expression/Expression'
import {IPackage}    from '../structure/Package'
import {IType}       from '../expression/type/Type'
import {
	IStatement,
	Statement,
	StatementType
}                    from './Statement'


export enum ReturnKind {
	ARRAY,
	BOOLEAN,
	BYTE,
	DATE,
	ERROR,
	INT64,
	NIL,
	DOES_NOT_RETURN,
	OBJECT,
	POINTER,
	STRING,
	TUPLE,
	VOID
}

export interface IReturnType {
	kind: ReturnKind
	type: IType
	subKinds: ReturnKind[]
	subTypes: IType[]

	equals(
		returnType: IReturnType
	): boolean

}

export class ReturnType {

	constructor(
		public kind: ReturnKind,
		public type: IType            = null,
		public subKinds: ReturnKind[] = null,
		public subTypes: IType[]      = null
	) {
		if (this.kind === ReturnKind.OBJECT
			|| this.kind === ReturnKind.POINTER) {
			if (!this.type) {
				throw new Error(`Class must be specified for ReturnKind.OBJECT`)
			}
		}
		if (this.subKinds) {
			if (!subTypes) {
				throw new Error(`subClasses Array must be specified if subKinds Array is specified.`)
			}
			subKinds.forEach((
				subKind,
				index
			) => {
				if (subKind === ReturnKind.OBJECT
					|| this.kind === ReturnKind.POINTER) {
					if (!subTypes[index]) {
						throw new Error(`subTypes[${index}] must be specified for subKinds[${index}] is OBJECT or POINTER`)
					}
				}
			})
		}
	}

	equals(
		returnType: IReturnType
	): boolean {
		if (this.kind !== returnType.kind) {
			return false
		}

		if (this.kind === ReturnKind.OBJECT) {
			// TODO: implement support for generics, once Go supports them
			return this.type.equals(returnType.type)
		}

		if (this.kind !== ReturnKind.TUPLE) {
			return true
		}

		if (this.subKinds.length !== returnType.subKinds.length) {
			return false
		}

		return this.subKinds.every((
			subKind,
			index
		) => {
			if (subKind !== returnType.subKinds[index]) {
				return false
			}
			if (this.subTypes[index]) {
				return returnType.subTypes[index].equals(this.subTypes[index])
			} else {
				return !returnType.subTypes[index]
			}
		})
	}
}

export const BOOLEAN         = new ReturnType(ReturnKind.BOOLEAN)
export const DOES_NOT_RETURN = new ReturnType(ReturnKind.DOES_NOT_RETURN)
export const ERROR           = new ReturnType(ReturnKind.ERROR)
export const BYTE           = new ReturnType(ReturnKind.BYTE)
export const INT64           = new ReturnType(ReturnKind.INT64)
export const NIL_RETURN      = new ReturnType(ReturnKind.NIL)
export const STRING          = new ReturnType(ReturnKind.STRING)
export const VOID            = new ReturnType(ReturnKind.VOID)

export interface IReturn
	extends IStatement {

	expression: IExpression

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class Return
	extends Statement
	implements IReturn {

	constructor(
		public expression: IExpression
	) {
		super(StatementType.RETURN)
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return this.expression.returnType(StatementType.RETURN)
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		return `return ${this.expression.src(packageOfSource, StatementType.RETURN)}`
	}

}

export function _return(
	expression: IExpression
) {
	return new Return(expression)
}