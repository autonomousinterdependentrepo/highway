import {IPackage} from '../structure/Package'
import {IElse}    from './Else'
import {IElseIf}  from './ElseIf'
import {IIf}      from './If'
import {
	DOES_NOT_RETURN,
	IReturnType
}                 from './Return'
import {
	IStatement,
	Statement,
	StatementType
}                 from './Statement'

export enum BlockType {
	BLOCK,
	ELSE,
	ELSE_IF,
	IF,
	FUNCTION
}

export interface IBlock
	extends IStatement {

	statements: (IStatement | (IIf | IElseIf | IElse)[])[]

	is(
		blockType: BlockType
	): boolean

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export abstract class Block
	extends Statement
	implements IBlock {

	constructor(
		statementType: StatementType,
		public statements: (IStatement | (IIf | IElseIf | IElse)[])[] = []
	) {
		super(statementType)

		for (const statement of statements) {
			if (!(statement instanceof Array)) {
				continue
			}
			if (!(statement[0] as IBlock).is(BlockType.IF)) {
				throw new Error(`Statement sub-arrays must be
If, ElseIf, Else combinations and must start with If.`)
			}
			for (let i = 1; i < statement.length - 1; i++) {
				if (!(statement[0] as IBlock).is(BlockType.ELSE_IF)) {
					throw new Error(`1 through N-1 Statements in sub-arrays must be ElseIf.`)
				}
			}
			if (!(statement[statement.length - 1] as IBlock).is(BlockType.ELSE_IF)
				&& !(statement[statement.length - 1] as IBlock).is(BlockType.ELSE)) {
				throw new Error(`Statement N in sub-arrays must be ElseIf or Else.`)
			}
		}
	}

	is(
		blockType: BlockType
	): boolean {
		return blockType === BlockType.BLOCK
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return DOES_NOT_RETURN
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		return `{
	${this.statements.map(
			statement => {
				if(statement instanceof Array) {
					return statement.map(aStatement => 
						aStatement.src(packageOfSource, StatementType.BLOCK)).join(`
`)
				} else {
					return statement.src(packageOfSource, StatementType.BLOCK)
				}
			}).join(`
`).split('\n').join('\n\t')}
}`
	}

}
