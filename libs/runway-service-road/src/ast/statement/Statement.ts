import {
	ASTUnit,
	IASTUnit
}                    from '../ASTUnit'
import {IPackage}    from '../structure/Package'
import {IReturnType} from './Return'

export enum StatementType {
	ASSIGNMENT,
	BOOLEAN,
	BLOCK,
	CALL,
	STRUCT_DEFINITION,
	DEFINITION,
	FILE_DEFINITION,
	PARAMETER_DEFINITION,
	RETURN_DEFINITION,
	RETURN
}

export interface IStatement
	extends IASTUnit {

	type: StatementType

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export abstract class Statement
	extends ASTUnit
	implements IStatement {

	constructor(
		public type: StatementType
	) {
		super()
	}

}
