import {IVariable}   from '../expression/constructs/Variable'
import {IExpression} from '../expression/Expression'
import {ASSIGN}      from '../operator/AssignOperator'
import {IPackage}    from '../structure/Package'
import {
	DOES_NOT_RETURN,
	IReturnType
}                    from './Return'
import {
	IStatement,
	Statement,
	StatementType
}                    from './Statement'

export interface IAssignment
	extends IStatement {

	asignee: IVariable | IVariable[],
	expression: IExpression

}

export class Assignment
	extends Statement
	implements IAssignment {

	constructor(
		public asignee: IVariable | IVariable[],
		public expression: IExpression
	) {
		super(StatementType.DEFINITION)
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return DOES_NOT_RETURN
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		let allVariablesDeclared, leftHand
		if (this.asignee instanceof Array) {
			allVariablesDeclared = this.asignee.every(
				variable => {
					let declared      = variable.declared
					variable.declared = true

					return declared
				})
			leftHand             = `${this.asignee.map(
				variable => variable.src(packageOfSource, StatementType.CALL))
				.join(', ')}`
		} else {
			allVariablesDeclared  = this.asignee.declared
			this.asignee.declared = true
			leftHand              = this.asignee.src(packageOfSource, statementType)
		}

		return `${leftHand} ${ASSIGN.src(packageOfSource, statementType, allVariablesDeclared
		)} ${this.expression.src(packageOfSource, statementType)}`
	}

}

export function assign(
	asignee: IVariable | IVariable[],
	expression: IExpression
): IAssignment {
	return new Assignment(asignee, expression)
}
