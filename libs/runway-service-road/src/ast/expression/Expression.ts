import {
	ASTUnit,
	IASTUnit
}                      from '../ASTUnit'
import {IReturnType}   from '../statement/Return'
import {StatementType} from '../statement/Statement'
import {IPackage}      from '../structure/Package'

export enum ExpressionType {
	BOOLEAN,
	FUNCTION_CALL,
	PRIMITIVE,
	VARIABLE,
	TYPE
}

export interface IExpression
	extends IASTUnit {

	expressionType: ExpressionType

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType,
		...additionalArgs: any[]
	): string

}

export abstract class Expression
	extends ASTUnit
	implements IExpression {

	constructor(
		public expressionType: ExpressionType
	) {
		super()
	}

}
