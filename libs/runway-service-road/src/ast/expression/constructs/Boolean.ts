import {IBooleanExpression} from '../BooleanExpression'
import {boolType}           from '../type/PrimitiveType'
import {
	IPrimitive,
	Primitive
}                           from './Primitive'

export interface IBoolean
	extends IPrimitive<boolean>,
	        IBooleanExpression<any, any> {

}

export class Boolean
	extends Primitive<boolean>
	implements IBoolean {

	leftExpr
	operator
	rightExpr
	expressionType

}


export function bool(
	value: boolean
): IBoolean {
	return new Boolean(boolType(), value)
}