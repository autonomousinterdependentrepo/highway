import {IReturnType}   from '../../statement/Return'
import {StatementType} from '../../statement/Statement'
import {IPackage}      from '../../structure/Package'
import {
	Expression,
	ExpressionType,
	IExpression
}                      from '../Expression'
import {
	byteType,
	int64Type,
	PrimitiveKind,
	PrimitiveType,
	stringType
} from '../type/PrimitiveType'

export interface IPrimitive<V extends boolean | number | string>
	extends IExpression {

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		sourcePackage: IPackage,
		statementType: StatementType
	): string

}

export class Primitive<V extends boolean | number | string>
	extends Expression
	implements IPrimitive<V> {

	constructor(
		public primitiveType: PrimitiveType,
		public value: V
	) {
		super(ExpressionType.PRIMITIVE)
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return this.primitiveType.returnType(statementType)
	}

	src(
		sourcePackage: IPackage,
		statementType: StatementType
	): string {
		switch (this.primitiveType.kind) {
			case PrimitiveKind.STRING:
				return `"${this.value}"`
			case PrimitiveKind.BOOL:
			case PrimitiveKind.INT64:
				return '' + this.value
			default:
				throw new Error(`Unknown PrimitiveKind ${this.primitiveType.kind}.`)
		}
	}

}

function primitive<V extends boolean | number | string>(
	primitiveType: PrimitiveType,
	value: V
): IPrimitive<V> {
	return new Primitive(primitiveType, value)
}

export function byte(
	value: number
): IPrimitive<number> {
	return primitive(byteType(), value)
}

export function int64(
	value: number
): IPrimitive<number> {
	return primitive(int64Type(), value)
}

export function string(
	value: number
): IPrimitive<string> {
	return primitive(stringType(), value)
}