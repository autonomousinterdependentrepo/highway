import {
	DOES_NOT_RETURN,
	IReturnType
}                           from '../../statement/Return'
import {StatementType}      from '../../statement/Statement'
import {IPackage}           from '../../structure/Package'
import {IBooleanExpression} from '../BooleanExpression'
import {
	Expression,
	ExpressionType,
	IExpression
}                           from '../Expression'
import {error}              from '../type/Error'
import {Nil}                from '../type/Nil'
import {IPointer}           from '../type/Pointer'
import {
	boolType,
	byteType,
	int64Type,
	stringType
}                           from '../type/PrimitiveType'
import {IType}              from '../type/Type'

export interface IVariable
	extends IExpression {

	annotation?: string
	declared: boolean
	name: string
	packag?: IPackage
	type: IType

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export interface IBooleanVariable
	extends IVariable,
	        IBooleanExpression<any, any> {

}

export class Variable
	extends Expression
	implements IVariable {

	declared = false

	constructor(
		public name: string,
		public type: IType,
		public packag?: IPackage,
		public annotation?: string,
	) {
		super(ExpressionType.VARIABLE)
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		switch (statementType) {
			case StatementType.ASSIGNMENT:
			case StatementType.DEFINITION:
			case StatementType.STRUCT_DEFINITION:
			case StatementType.PARAMETER_DEFINITION:
			case StatementType.RETURN_DEFINITION:
				return DOES_NOT_RETURN
			case StatementType.BOOLEAN:
			case StatementType.CALL:
			case StatementType.RETURN:
				return this.type.returnType(statementType)
			default:
				throw new Error(`Unknown StatementType: ${statementType}`)
		}
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		switch (statementType) {
			case StatementType.ASSIGNMENT:
			case StatementType.BOOLEAN:
				return this.name
			case StatementType.DEFINITION:
				return `var ${this.name} ${this.type.src(packageOfSource, statementType)}`
			case StatementType.STRUCT_DEFINITION:
				let annotation = ''
				if (this.annotation) {
					annotation = ` \`${this.annotation}\``
				}
				return `${this.name} ${this.type.src(packageOfSource, StatementType.DEFINITION)}${annotation}`
			case StatementType.PARAMETER_DEFINITION:
				return `${this.name} ${this.type.src(packageOfSource, statementType)}`
			case StatementType.CALL:
			case StatementType.RETURN:
				let prefix = ''
				if (this.packag) {
					prefix = `${this.packag.name}.`
				}
				return prefix + this.name
			case StatementType.RETURN_DEFINITION:
				throw new Error(`Variables are not supported in Return Definitions`)
			default:
				throw new Error(`Unknown StatementType: ${statementType}`)
		}
	}

}

export function boolVar(
	name: string,
	packag?: IPackage
): IBooleanVariable {
	return variable(name, boolType(), packag) as any
}

export function byteVar(
	name: string,
	packag?: IPackage
): IVariable {
	return variable(name, byteType(), packag)
}

export function int64Var(
	name: string,
	packag?: IPackage
): IVariable {
	return variable(name, int64Type(), packag)
}

export function stringVar(
	name: string,
	packag?: IPackage
): IVariable {
	return variable(name, stringType(), packag)
}

export function classVar(
	name: string,
	clas: IType,
	packag?: IPackage
): IVariable {
	return variable(name, clas, packag)
}

export function pointerVar<T extends IType>(
	name: string,
	pointer: IPointer<T>,
	packag?: IPackage
): IVariable {
	return variable(name, pointer, packag)
}

export function errorVar(
	name: string,
	sourcePackage?: IPackage
): IVariable {
	return variable(name, error(), sourcePackage)
}

export function variable(
	name: string,
	type: IType,
	sourcePackage?: IPackage,
	annotation?: string,
): IVariable {
	return new Variable(name, type, sourcePackage, annotation)
}

export const NIL = variable('nil', new Nil())