import {IASTUnit}      from '../../ASTUnit'
import {
	IReturnType,
	ReturnKind,
	ReturnType
}                      from '../../statement/Return'
import {StatementType} from '../../statement/Statement'
import {IPackage}      from '../../structure/Package'
import {
	ExpressionType,
	IExpression
}                      from '../Expression'
import {
	IType,
	Type
}                      from './Type'

export enum TupleType {
	DEFINITION,
	EXECUTION
}

export interface ITuple
	extends IType {

	type: TupleType,
	members: IASTUnit[]

	equals(
		type: IType
	): boolean

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class Tuple
	extends Type
	implements ITuple {

	constructor(
		public type: TupleType,
		public members: IASTUnit[]
	) {
		super(ExpressionType.TYPE)
	}

	equals(
		type: IType
	): boolean {
		return type instanceof Tuple
			&& type.members.length === this.members.length
			&& this.returnType(StatementType.RETURN)
				.equals(type.returnType(StatementType.RETURN))
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		let memberReturnKinds: ReturnKind[] = []
		let memberTypes: IType[]            = []

		this.members.forEach(
			member => {
				const memberReturnType = member.returnType(statementType)
				memberReturnKinds.push(memberReturnType.kind)
				memberTypes.push(memberReturnType.type)
			})
		return new ReturnType(ReturnKind.TUPLE, null, memberReturnKinds, memberTypes)
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		switch (this.type) {
			case TupleType.DEFINITION:
				return `(${this.members.map(
					member => member.src(packageOfSource, statementType)).join(', ')})`
			case TupleType.EXECUTION:
				return `${this.members.map(
					member => member.src(packageOfSource, statementType)).join(', ')}`
			default:
				throw new Error(`Unknown TypeType ${this.type}.`)
		}
	}
}

function tupleImpl(
	type: TupleType,
	...members: IExpression[]
): ITuple {
	return new Tuple(type, members)
}

export function tupleDef(
	...members: IType[]
): ITuple {
	return tupleImpl(TupleType.DEFINITION, ...members)
}

export function tuple<E1 extends IExpression, E2 extends IExpression>(
	expression1: E1,
	expression2: E2,
): ITuple
export function tuple<E1 extends IExpression, E2 extends IExpression, E3 extends IExpression>(
	expression1: E1,
	expression2: E2,
	expression3: E3,
): ITuple
export function tuple<E1 extends IExpression, E2 extends IExpression, E3 extends IExpression, E4 extends IExpression>(
	expression1: E1,
	expression2: E2,
	expression3: E3,
	expression4: E4,
): ITuple
export function tuple<E1 extends IExpression, E2 extends IExpression, E3 extends IExpression, E4 extends IExpression, E5 extends IExpression>(
	expression1: E1,
	expression2: E2,
	expression3: E3,
	expression4: E4,
	expression5: E5,
): ITuple
export function tuple(
	...members: any[]
): ITuple {
	return tupleImpl(TupleType.EXECUTION, ...members)
}
