import {
	IReturnType,
	ReturnKind,
	ReturnType
}                       from '../../statement/Return'
import {StatementType}  from '../../statement/Statement'
import {IPackage}       from '../../structure/Package'
import {IVariable}      from '../constructs/Variable'
import {ExpressionType} from '../Expression'
import {
	IType,
	Type
}                       from './Type'

export interface IStruct
	extends IType {

	name: string,
	sourcePackage: IPackage,
	state: IVariable[]

	equals(
		type: IType
	): boolean

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class Struct
	extends Type
	implements IStruct {

	typeOfReturn: IReturnType

	constructor(
		public name: string,
		public sourcePackage: IPackage,
		public state: IVariable[] = [],
	) {
		super(ExpressionType.TYPE)

		// TODO: add support for generics once they make it into go
		this.typeOfReturn = new ReturnType(ReturnKind.OBJECT, this)
	}

	equals(
		type: IType
	): boolean {
		return type instanceof Struct
			&& this.name === type.name
			&& this.sourcePackage.equals(type.sourcePackage)
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return this.typeOfReturn
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		switch (statementType) {
			case StatementType.ASSIGNMENT:
				return ``
			case StatementType.STRUCT_DEFINITION: {
				let stateDef = ''
				if(this.state.length) {
					stateDef += '\n'
				}
				for (let property of this.state) {
					stateDef += `\t${property.src(packageOfSource, statementType)}\n`
				}
				return `type ${this.name} struct {${stateDef}
}`
			}
			case StatementType.BOOLEAN:
			case StatementType.DEFINITION:
			case StatementType.PARAMETER_DEFINITION:
			case StatementType.RETURN:
			case StatementType.RETURN_DEFINITION:
				let packagePrefix = ''
				if (!packageOfSource.equals(this.sourcePackage)) {
					packagePrefix = `${this.sourcePackage.name}.`
				}
				return `${packagePrefix}${this.name}`
			default:
				throw new Error(`Unexpected StatementType ${statementType}.`)
		}
	}

}

export function struct(
	name: string,
	sourcePackage: IPackage,
	state?: IVariable[],
): IStruct {
	return new Struct(name, sourcePackage, state)
}
