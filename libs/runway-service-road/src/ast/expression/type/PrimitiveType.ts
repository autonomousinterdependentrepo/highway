import {
	BOOLEAN,
	BYTE,
	INT64,
	IReturnType,
	STRING
}                       from '../../statement/Return'
import {StatementType}  from '../../statement/Statement'
import {IPackage}       from '../../structure/Package'
import {ExpressionType} from '../Expression'
import {
	IType,
	Type
}                       from './Type'

export enum PrimitiveKind {
	BOOL,
	BYTE,
	INT64,
	STRING
}

export interface IPrimitiveType
	extends IType {

	kind: PrimitiveKind

	equals(
		type: IType
	): boolean

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		sourcePackage: IPackage,
		statementType: StatementType
	): string

}

export class PrimitiveType
	extends Type
	implements IPrimitiveType {

	constructor(
		public kind: PrimitiveKind,
	) {
		super(ExpressionType.TYPE)
	}

	equals(
		type: IType
	): boolean {
		return type instanceof PrimitiveType
			&& type.kind === this.kind
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		switch (this.kind) {
			case PrimitiveKind.BOOL:
				return BOOLEAN
			case PrimitiveKind.BYTE:
				return BYTE
			case PrimitiveKind.INT64:
				return INT64
			case PrimitiveKind.STRING:
				return STRING
			default:
				throw new Error(`Unknown PrimitiveKind ${this.kind}.`)
		}
	}

	src(
		sourcePackage: IPackage,
		statementType: StatementType
	): string {
		switch (this.kind) {
			case PrimitiveKind.BOOL:
				return 'bool'
			case PrimitiveKind.BYTE:
				return 'byte'
			case PrimitiveKind.INT64:
				return 'int64'
			case PrimitiveKind.STRING:
				return 'string'
			default:
				throw new Error(`Unknown PrimitiveKind ${this.kind}.`)
		}
	}

}

function primitiveType(
	kind: PrimitiveKind
): IPrimitiveType {
	return new PrimitiveType(kind)
}

export function boolType(): IPrimitiveType {
	return primitiveType(PrimitiveKind.BOOL)
}

export function byteType(): IPrimitiveType {
	return primitiveType(PrimitiveKind.BYTE)
}

export function int64Type(): IPrimitiveType {
	return primitiveType(PrimitiveKind.INT64)
}

export function stringType(): IPrimitiveType {
	return primitiveType(PrimitiveKind.STRING)
}