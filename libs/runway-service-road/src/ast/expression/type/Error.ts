import {
	ERROR,
	IReturnType
}                       from '../../statement/Return'
import {StatementType}  from '../../statement/Statement'
import {IPackage}       from '../../structure/Package'
import {ExpressionType} from '../Expression'
import {
	IType,
	Type
}                       from './Type'

export interface IError
	extends IType {

	equals(
		type: IType
	): boolean

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string
}

export class Error
	extends Type
	implements IError {

	constructor() {
		super(ExpressionType.TYPE)
	}

	equals(
		type: IType
	): boolean {
		return type instanceof Error
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		return 'error'
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return ERROR
	}

}

export function error(): IError {
	return new Error()
}
