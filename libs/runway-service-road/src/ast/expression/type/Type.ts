import {IReturnType}   from '../../statement/Return'
import {StatementType} from '../../statement/Statement'
import {IPackage}      from '../../structure/Package'
import {
	Expression,
	IExpression
}                      from '../Expression'

export interface IType
	extends IExpression {

	returnType(
		statementType: StatementType
	): IReturnType

	equals(
		type: IType
	): boolean

	src(
		packageOfSource: IPackage,
		statementType: StatementType,
		...additionalArgs: any[]
	): string

}

export abstract class Type
	extends Expression {

}
