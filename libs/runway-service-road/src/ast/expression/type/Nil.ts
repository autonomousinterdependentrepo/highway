import {
	IReturnType,
	NIL_RETURN
}                       from '../../statement/Return'
import {StatementType}  from '../../statement/Statement'
import {IPackage}       from '../../structure/Package'
import {ExpressionType} from '../Expression'
import {
	IType,
	Type
}                       from './Type'

export interface INil
	extends IType {

}

export class Nil
	extends Type
	implements INil {

	constructor() {
		super(ExpressionType.TYPE)
	}

	equals(
		type: IType
	): boolean {
		return type instanceof Nil
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return NIL_RETURN
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		return 'nil'
	}

}
