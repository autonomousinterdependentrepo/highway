import {
	IReturnType,
	ReturnKind,
	ReturnType
}                       from '../../statement/Return'
import {StatementType}  from '../../statement/Statement'
import {IPackage}       from '../../structure/Package'
import {ExpressionType} from '../Expression'
import {
	IType,
	Type
}                       from './Type'

export interface IArray<T extends IType>
	extends IType {

	ofType: T

	equals(
		type: IType
	): boolean

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class Array<T extends IType>
	extends Type
	implements IArray<T> {

	constructor(
		public ofType: T
	) {
		super(ExpressionType.TYPE)
	}

	equals(
		type: IType
	): boolean {
		// TODO: add support for generics, once Go has them
		return type instanceof Array
			&& type.ofType.equals(this.ofType)
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		// TODO: add support for generics, once Go has them

		return new ReturnType(ReturnKind.ARRAY, this.ofType)
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		return `[]${this.ofType.src(packageOfSource, statementType)}`
	}

}

export function array<T extends IType>(
	ofType: T
): IArray<T> {
	return new Array<T>(ofType)
}
