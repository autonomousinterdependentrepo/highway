import {
	IReturnType,
	ReturnKind,
	ReturnType
}                       from '../../statement/Return'
import {StatementType}  from '../../statement/Statement'
import {IPackage}       from '../../structure/Package'
import {ExpressionType} from '../Expression'
import {
	IType,
	Type
}                       from './Type'

export interface IPointer<T extends IType>
	extends IType {

	ofType: T

	equals(
		type: IType
	): boolean

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class Pointer<T extends IType>
	extends Type
	implements IPointer<T> {

	constructor(
		public ofType: T
	) {
		super(ExpressionType.TYPE)
	}

	equals(
		type: IType
	): boolean {
		// TODO: add support for generics, once Go has them
		return type instanceof Pointer
			&& type.ofType.equals(this.ofType)
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		// TODO: add support for generics, once Go has them

		return new ReturnType(ReturnKind.POINTER, this.ofType)
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		return `*${this.ofType.src(packageOfSource, statementType)}`
	}

}

export function ptr<T extends IType>(
	ofType: T
): IPointer<T> {
	return new Pointer<T>(ofType)
}
