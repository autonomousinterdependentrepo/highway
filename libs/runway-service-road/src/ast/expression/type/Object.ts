import {
	IReturnType,
	ReturnKind,
	ReturnType
}                       from '../../statement/Return'
import {StatementType}  from '../../statement/Statement'
import {IPackage}       from '../../structure/Package'
import {ExpressionType} from '../Expression'
import {IStruct}        from './Struct'
import {
	IType,
	Type
}                       from './Type'

export interface IObject
	extends IType {

	typeOfReturn: IReturnType

	equals(
		type: IType
	): boolean

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class Object
	extends Type
	implements IObject {

	constructor(
		public struct: IStruct,
		public typeOfReturn: IReturnType = null,
	) {
		super(ExpressionType.TYPE)

		// TODO: add support for generics once they make it into go
		this.typeOfReturn = new ReturnType(ReturnKind.OBJECT, this)
	}

	equals(
		type: IType
	): boolean {
		return type instanceof Object && this.struct.equals(type.struct)
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return this.typeOfReturn
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType,
	): string {
		throw new Error('Not implemented')
	}

}
