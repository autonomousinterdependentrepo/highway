import {IFunction}          from '../statement/Function'
import {
	IReturnType,
	VOID
}                           from '../statement/Return'
import {StatementType}      from '../statement/Statement'
import {IFunctionReference} from '../structure/FunctionReference'
import {IPackage}           from '../structure/Package'
import {IVariable}          from './constructs/Variable'
import {
	Expression,
	ExpressionType,
	IExpression
}                           from './Expression'

export interface IFunctionCall
	extends IExpression {

	func: IFunction | IFunctionReference
	funcParams: IVariable[]
	params: IExpression[]

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class FunctionCall
	extends Expression
	implements IFunctionCall {

	funcParams: IVariable[]

	constructor(
		public func: IFunction | IFunctionReference,
		public params: IExpression[]
	) {
		super(ExpressionType.FUNCTION_CALL)

		if (func instanceof Function) {
			this.funcParams = (func as IFunction).paramDefs.parameters
		} else {
			this.funcParams = (func as IFunctionReference).parameters
		}

		if (params.length !== this.funcParams.length) {
			throw new Error(`Number of parameters does not match for function call "${func.name}".
Expecting ${this.funcParams.length} and returned ${params.length}`)
		}
		params.forEach((
			param,
			index
		) => {
			if (!param.returnType(StatementType.CALL).equals(this.funcParams[index].returnType(StatementType.CALL))) {
				throw new Error(`Unexpected return type for parameter[${index}]
of function call "${func.name}".`)
			}
		})
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		if (!this.func.typeOfReturn) {
			return VOID
		}
		return this.func.typeOfReturn.returnType(StatementType.RETURN_DEFINITION)
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		let packagePrefix = ''
		if (!packageOfSource.equals(this.func.sourcePackage)) {
			packagePrefix = `${this.func.sourcePackage.name}.`
		}

		return `${packagePrefix}${this.func.name}(${this.params.map(
			param => param.src(packageOfSource, StatementType.CALL)).join(', ')})`
	}

}

export function call(
	func: IFunction | IFunctionReference,
	params: IExpression[]
): IFunctionCall {
	return new FunctionCall(func, params)
}
