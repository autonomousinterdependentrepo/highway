import {
	EQUALS,
	GREATER_THAN,
	GREATER_THAN_OR_EQUALS,
	ICompareOperator,
	LESS_THAN,
	LESS_THAN_OR_EQUALS,
	NOT_EQUALS
}                      from '../operator/CompareOperator'
import {
	BOOLEAN,
	IReturnType
}                      from '../statement/Return'
import {StatementType} from '../statement/Statement'
import {IPackage}      from '../structure/Package'
import {
	BooleanExpression,
	IBooleanExpression
}                      from './BooleanExpression'
import {IExpression}   from './Expression'

export interface IComparisonExpression
	extends IBooleanExpression<IExpression, ICompareOperator> {

	returnType(
		statementType: StatementType
	): IReturnType

}

export class ComparisonExpression
	extends BooleanExpression<IExpression, ICompareOperator>
	implements IComparisonExpression {

	constructor(
		leftExpr: IExpression,
		operator: ICompareOperator,
		rightExpr: IExpression
	) {
		super(leftExpr, operator, rightExpr)
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return BOOLEAN
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		return this.leftExpr.src(packageOfSource, statementType) + ' '
			+ this.operator.src(packageOfSource, statementType) + ' '
			+ this.rightExpr.src(packageOfSource, statementType)
	}

}

function comparisonExpression<L extends IExpression, R extends IExpression>(
	left: L,
	operator: ICompareOperator,
	right: R
) {
	return new ComparisonExpression(left, operator, right)
}

export function equals<L extends IExpression, R extends IExpression>(
	left: L,
	right: R
): IComparisonExpression {
	return comparisonExpression(left, EQUALS, right)
}

export function greaterThan<L extends IExpression, R extends IExpression>(
	left: L,
	right: R
): IComparisonExpression {
	return comparisonExpression(left, GREATER_THAN, right)
}

export function greaterThanOrEquals<L extends IExpression, R extends IExpression>(
	left: L,
	right: R
): IComparisonExpression {
	return comparisonExpression(left, GREATER_THAN_OR_EQUALS, right)
}

export function lessThan<L extends IExpression, R extends IExpression>(
	left: L,
	right: R
): IComparisonExpression {
	return comparisonExpression(left, LESS_THAN, right)
}

export function lessThanOrEquals<L extends IExpression, R extends IExpression>(
	left: L,
	right: R
): IComparisonExpression {
	return comparisonExpression(left, LESS_THAN_OR_EQUALS, right)
}

export function notEquals<L extends IExpression, R extends IExpression>(
	left: L,
	right: R
): IComparisonExpression {
	return comparisonExpression(left, NOT_EQUALS, right)
}
