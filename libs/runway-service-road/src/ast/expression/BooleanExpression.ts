import {
	AND,
	BooleanOperatorType,
	IBooleanOperator,
	NOT,
	OR
}                      from '../operator/BooleanOperator'
import {IOperator}     from '../operator/Operator'
import {
	BOOLEAN,
	IReturnType
}                      from '../statement/Return'
import {StatementType} from '../statement/Statement'
import {IPackage}      from '../structure/Package'
import {
	Expression,
	ExpressionType,
	IExpression
}                      from './Expression'

export interface IBooleanExpression<E extends IExpression, O extends IOperator>
	extends IExpression {

	leftExpr: E,
	operator: O,
	rightExpr: E

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class BooleanExpression<E extends IExpression, O extends IOperator>
	extends Expression
	implements IBooleanExpression<E, O> {

	constructor(
		public leftExpr: E,
		public operator: O,
		public rightExpr: E = null
	) {
		super(ExpressionType.BOOLEAN)

	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return BOOLEAN
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		if (!BOOLEAN.equals(this.leftExpr.returnType(StatementType.BOOLEAN))) {
			throw new Error(`Left BooleanExpression ReturnType must be BOOLEAN`)
		}
		if (!this.operator) {
			throw new Error(`Cannot specify a BooleanExpression without a Boolean Operator.`)
		}
		switch ((this.operator as any as IBooleanOperator).bType) {
			case BooleanOperatorType.NOT:
				if (this.rightExpr) {
					throw new Error(`Cannot specify Right BooleanExpression with the "!" Boolean Operator.`)
				}
				return this.operator.src(packageOfSource, statementType)
					+ this.leftExpr.src(packageOfSource, statementType)
			case BooleanOperatorType.AND:
			case BooleanOperatorType.OR:
				if (!this.rightExpr) {
					throw new Error(`Must specify Right BooleanExpression with the "&&" and "||" Boolean Operators.`)
				}
				if (!this.rightExpr.returnType(StatementType.BOOLEAN).equals(BOOLEAN)) {
					throw new Error(`ReturnType of Right BooleanExpression must be Boolean`)
				}
				if (!BOOLEAN.equals(this.rightExpr.returnType(StatementType.BOOLEAN))) {
					throw new Error(`Right BooleanExpression ReturnType must be BOOLEAN`)
				}
				return this.leftExpr.src(packageOfSource, statementType) + ' '
					+ this.operator.src(packageOfSource, statementType) + ' '
					+ this.rightExpr.src(packageOfSource, statementType)
			default:
				throw new Error(`Uknown BooleanOperatorType ${(this.operator as any as IBooleanOperator).bType} `)
		}
	}

}

function boolExpr(
	leftBoolExpr: IBooleanExpression<any, any>,
	boolOperator: IBooleanOperator,
	rightBoolExpr?: IBooleanExpression<any, any>
): IBooleanExpression<IBooleanExpression<any, any>, IBooleanOperator> {
	return new BooleanExpression(leftBoolExpr, boolOperator, rightBoolExpr)
}

export function not(
	leftBoolExpr: IBooleanExpression<any, any>
): IBooleanExpression<IBooleanExpression<any, any>, IBooleanOperator> {
	return new BooleanExpression(leftBoolExpr, NOT())
}

export function and(
	leftBoolExpr: IBooleanExpression<any, any>,
	rightBoolExpr: IBooleanExpression<any, any>
): IBooleanExpression<IBooleanExpression<any, any>, IBooleanOperator> {
	return new BooleanExpression(leftBoolExpr, AND(), rightBoolExpr)
}

export function or(
	leftBoolExpr: IBooleanExpression<any, any>,
	rightBoolExpr: IBooleanExpression<any, any>
): IBooleanExpression<IBooleanExpression<any, any>, IBooleanOperator> {
	return new BooleanExpression(leftBoolExpr, OR(), rightBoolExpr)
}

