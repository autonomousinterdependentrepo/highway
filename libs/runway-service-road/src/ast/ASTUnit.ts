import {IReturnType}   from './statement/Return'
import {StatementType} from './statement/Statement'
import {IPackage}      from './structure/Package'

export interface IASTUnit {

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType,
		...additionalArgs: any[]
	): string

}

export abstract class ASTUnit
	implements IASTUnit {

	abstract returnType(
		statementType: StatementType
	): IReturnType

	abstract src(
		packageOfSource: IPackage,
		statementType: StatementType,
		...additionalArgs: any[]
	): string;

}
