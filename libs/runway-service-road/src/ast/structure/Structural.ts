import {
	ASTUnit,
	IASTUnit
}                      from '../ASTUnit'
import {
	DOES_NOT_RETURN,
	IReturnType
}                      from '../statement/Return'
import {StatementType} from '../statement/Statement'
import {IPackage}      from './Package'

export interface IStructural
	extends IASTUnit {

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType,
		...additionalArgs: any[]
	): string

}

export abstract class Structural
	extends ASTUnit
	implements IStructural {

	returnType(
		statementType: StatementType
	): IReturnType {
		return DOES_NOT_RETURN
	}

}
