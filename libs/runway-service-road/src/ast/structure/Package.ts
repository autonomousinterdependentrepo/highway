import {StatementType} from '../statement/Statement'
import {
	IStructural,
	Structural
}                      from './Structural'

export interface IPackage
	extends IStructural {

	name: string
	path: string

	src(
		sourcePackage: IPackage,
		statementType: StatementType
	): string

	equals(
		packag: IPackage
	): boolean

	fullPath(): string

}

export class Package
	extends Structural
	implements IPackage {

	constructor(
		public name: string,
		public path: string = ''
	) {
		super()
	}

	src(
		sourcePackage: IPackage,
		statementType: StatementType
	): string {
		return `package ${this.name}`
	}

	equals(
		sourcePackage: IPackage
	): boolean {
		return this.fullPath() === sourcePackage.fullPath()
	}

	fullPath(): string {
		return `${this.path}/${this.name}`;
	}

}

export function packag(
	name: string,
	path?: string
): IPackage {
	return new Package(name, path)
}
