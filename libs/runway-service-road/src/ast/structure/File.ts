import {
	IStatement,
	StatementType
}                 from '../statement/Statement'
import {IImport}  from './Import'
import {IPackage} from './Package'
import {
	IStructural,
	Structural
}                 from './Structural'

export interface IFile
	extends IStructural {

	imports: IImport[]
	name: string,
	packag: IPackage
	statements: IStatement[]

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class File
	extends Structural
	implements IFile {

	constructor(
		public name: string,
		public packag: IPackage,
		public imports: IImport[],
		public statements: IStatement[] = []
	) {
		super()
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		let imports = ''
		if (this.imports.length) {
			imports = `
import (
${this.imports.map(
				imprt => `${imprt.src(packageOfSource, statementType)}`).join(`
`)}
)
`
		}
		return `${this.packag.src(packageOfSource, statementType)}
${imports}
${this.statements.map(statement => statement.src(packageOfSource, statementType)).join(`
`)}`
	}

}

export function file(
	name: string,
	sourcePackage: IPackage,
	imports: IImport[],
	statements?: IStatement[]
): IFile {
	return new File(
		name,
		sourcePackage,
		imports,
		statements
	)
}
