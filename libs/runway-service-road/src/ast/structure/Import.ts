import {StatementType} from '../statement/Statement'
import {IPackage}      from './Package'
import {
	IStructural,
	Structural
}                      from './Structural'

export interface IImport
	extends IStructural {

	packag: IPackage

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class Import
	extends Structural
	implements IImport {

	constructor(
		public packag: IPackage
	) {
		super()
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		if(packageOfSource.equals(this.packag)) {
			throw new Error(`Cannot import package "${this.packag.fullPath()}" into itself.`)
		}
		return `	"${this.packag.fullPath()}"`
	}

}

export function imprt(
	packag: IPackage
): IImport {
	return new Import(packag)
}
