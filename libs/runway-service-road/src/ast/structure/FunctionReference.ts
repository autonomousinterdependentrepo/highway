import {IVariable}     from '../expression/constructs/Variable'
import {IType}         from '../expression/type/Type'
import {StatementType} from '../statement/Statement'
import {IPackage}      from './Package'
import {
	IStructural,
	Structural
}                      from './Structural'

export interface IFunctionReference
	extends IStructural {

	sourcePackage: IPackage
	name: string,
	parameters: IVariable[]
	typeOfReturn: IType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class FunctionReference
	extends Structural
	implements IFunctionReference {

	constructor(
		public sourcePackage: IPackage,
		public name: string,
		public parameters: IVariable[] = [],
		public typeOfReturn: IType     = null
	) {
		super()
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		throw new Error(`Function References cannot be output to source.`)
	}

}

export function funcRef(
	packag: IPackage,
	name: string,
	parameters?: IVariable[],
	typeOfReturn?: IType
): IFunctionReference {
	return new FunctionReference(packag, name, parameters, typeOfReturn)
}
