import {StatementType} from '../statement/Statement'
import {IPackage}      from '../structure/Package'
import {
	IOperator,
	Operator,
	OperatorType
}                      from './Operator'

export interface IAssignOperator
	extends IOperator {

	src(
		packageOfSource: IPackage,
		statementType: StatementType,
		allVariablesDeclared: boolean
	): string

}

export class AssignOperator
	extends Operator
	implements IAssignOperator {

	constructor() {
		super(OperatorType.ASSIGN)
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType,
		allVariablesDeclared: boolean
	): string {
		return allVariablesDeclared ? '=' : ':='
	}

}

export const ASSIGN = new AssignOperator()