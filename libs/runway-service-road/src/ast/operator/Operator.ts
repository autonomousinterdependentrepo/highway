import {
	ASTUnit,
	IASTUnit
}                      from '../ASTUnit'
import {
	DOES_NOT_RETURN,
	IReturnType
}                      from '../statement/Return'
import {StatementType} from '../statement/Statement'
import {IPackage}      from '../structure/Package'

export enum OperatorType {
	ASSIGN,
	BINARY,
	BOOLEAN,
	COMPARE
}

export interface IOperator
	extends IASTUnit {

	type: OperatorType

	returnType(
		statementType: StatementType
	): IReturnType

	src(
		packageOfSource: IPackage,
		statementType: StatementType,
		...additionalArgs: any[]
	): string

}

export abstract class Operator
	extends ASTUnit
	implements IOperator {


	constructor(
		public type: OperatorType
	) {
		super()
	}

	returnType(
		statementType: StatementType
	): IReturnType {
		return DOES_NOT_RETURN
	}

}
