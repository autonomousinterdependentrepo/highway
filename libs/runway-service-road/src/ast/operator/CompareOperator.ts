import {StatementType} from '../statement/Statement'
import {IPackage}      from '../structure/Package'
import {
	IOperator,
	Operator,
	OperatorType
}                      from './Operator'

export enum CompareOperatorType {
	EQUALS,
	GREATER_THAN,
	GREATER_THAN_OR_EQUALS,
	LESS_THAN,
	LESS_THAN_OR_EQUALS,
	NOT_EQUALS,
}

export interface ICompareOperator
	extends IOperator {

	cType: CompareOperatorType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class CompareOperator
	extends Operator
	implements ICompareOperator {

	constructor(
		public cType: CompareOperatorType
	) {
		super(OperatorType.COMPARE)
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		switch (this.cType) {
			case CompareOperatorType.EQUALS:
				return '=='
			case CompareOperatorType.GREATER_THAN:
				return '>'
			case CompareOperatorType.GREATER_THAN_OR_EQUALS:
				return '>='
			case CompareOperatorType.LESS_THAN:
				return '<'
			case CompareOperatorType.LESS_THAN_OR_EQUALS:
				return '<='
			case CompareOperatorType.NOT_EQUALS:
				return '!='
			default:
				throw new Error(`Uknown CompareOperatorType ${this.cType} `)
		}
	}

}

function compareOperator(
	cType: CompareOperatorType
): ICompareOperator {
	return new CompareOperator(cType)
}

export const EQUALS = compareOperator(CompareOperatorType.EQUALS)
export const GREATER_THAN = compareOperator(CompareOperatorType.GREATER_THAN)
export const GREATER_THAN_OR_EQUALS = compareOperator(CompareOperatorType.GREATER_THAN_OR_EQUALS)
export const LESS_THAN = compareOperator(CompareOperatorType.LESS_THAN)
export const LESS_THAN_OR_EQUALS = compareOperator(CompareOperatorType.LESS_THAN_OR_EQUALS)
export const NOT_EQUALS = compareOperator(CompareOperatorType.NOT_EQUALS)