import {StatementType} from '../statement/Statement'
import {IPackage}      from '../structure/Package'
import {
	IOperator,
	Operator,
	OperatorType
}                      from './Operator'

export enum BooleanOperatorType {
	AND,
	NOT,
	OR
}

export interface IBooleanOperator
	extends IOperator {

	bType: BooleanOperatorType

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string

}

export class BooleanOperator
	extends Operator
	implements IBooleanOperator {

	constructor(
		public bType: BooleanOperatorType
	) {
		super(OperatorType.BOOLEAN)
	}

	src(
		packageOfSource: IPackage,
		statementType: StatementType
	): string {
		switch (this.bType) {
			case BooleanOperatorType.AND:
				return '&&'
			case BooleanOperatorType.NOT:
				return '!'
			case BooleanOperatorType.OR:
				return '||'
			default:
				throw new Error(`Uknown BooleanOperatorType ${this.bType} `)
		}
	}

}

export function NOT(): IBooleanOperator {
	return new BooleanOperator(BooleanOperatorType.NOT)
}

export function AND(): IBooleanOperator {
	return new BooleanOperator(BooleanOperatorType.AND)
}

export function OR(): IBooleanOperator {
	return new BooleanOperator(BooleanOperatorType.OR)
}